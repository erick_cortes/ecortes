"""techk URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from school.views import (
    CoursesList,
    CourseDetail,
    CourseCreate,
    CourseDelete,
    CourseUpdate,
    TeachersList,
    TeacherDetail,
    TeacherCreate,
    TeacherDelete,
    TeacherUpdate,
    StudentsList,
    StudentDetail,
    StudentCreate,
    StudentDelete,
    StudentUpdate,
    TestsList,
    TestCreate,
    TestDelete,
    TestDetail,
    TestUpdate,
    GradesList,
    GradeCreate,
    GradeDetail,
    GradeUpdate,
    GradeDelete
)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^courses/', CoursesList.as_view(), name='listcourse'),
    url(r'^coursedetail/(?P<pk>\d+)$', CourseDetail.as_view(), name='detailcourse'),
    url(r'^newcourse$', CourseCreate.as_view(), name='newcourse'),
    url(r'^editcourse/(?P<pk>\d+)$', CourseUpdate.as_view(), name='editcourse'),
    url(r'^deletecourse/(?P<pk>\d+)$', CourseDelete.as_view(), name='deletecourse'),
    url(r'^students$', StudentsList.as_view(), name='liststudent'),
    url(r'^studentdetail/(?P<pk>\d+)$', StudentDetail.as_view(), name='detailstudent'),
    url(r'^newstudent$', StudentCreate.as_view(), name='newstudent'),
    url(r'^editstudent/(?P<pk>\d+)$', StudentUpdate.as_view(), name='editstudent'),
    url(r'^deletestudent/(?P<pk>\d+)$', StudentDelete.as_view(), name='deletestudent'),
    url(r'^teachers$', TeachersList.as_view(), name='listteacher'),
    url(r'^teacherdetail/(?P<pk>\d+)$', TeacherDetail.as_view(), name='detailteacher'),
    url(r'^newteacher$', TeacherCreate.as_view(), name='newteacher'),
    url(r'^editteacher/(?P<pk>\d+)$', TeacherUpdate.as_view(), name='editteacher'),
    url(r'^deleteteacher/(?P<pk>\d+)$', TeacherDelete.as_view(), name='deleteteacher'),
    url(r'^tests$', TestsList.as_view(), name='listtest'),
    url(r'^testdetail/(?P<pk>\d+)$', TestDetail.as_view(), name='detailtest'),
    url(r'^newtest$', TestCreate.as_view(), name='newtest'),
    url(r'^edittest/(?P<pk>\d+)$', TestUpdate.as_view(), name='edittest'),
    url(r'^deletetest/(?P<pk>\d+)$', TestDelete.as_view(), name='deletetest'),
    url(r'^grades$', GradesList.as_view(), name='listgrade'),
    url(r'^gradedetail/(?P<pk>\d+)$', GradeDetail.as_view(), name='detailgrade'),
    url(r'^newgrade/$', GradeCreate.as_view(), name='newgrade'),
    url(r'^editgrade/(?P<pk>\d+)$', GradeUpdate.as_view(), name='editgrade'),
    url(r'^deletegrades/(?P<pk>\d+)$', GradeDelete.as_view(), name='deletegrade'),
]
