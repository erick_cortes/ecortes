# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from decimal import Decimal

# Create your models here.

class Student(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

    #promedio de todas las pruebas
    def testavg(self):
        avg = 0
        val = 0
        cant = self.grade_set.all().count()
        for grade in self.grade_set.all():
            val += grade.value

        if cant is not 0:
            avg = val / cant

        return avg


class Teacher(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

class Course(models.Model):
    name = models.CharField(max_length=30)
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)
    students = models.ManyToManyField(Student)

    def __unicode__(self):
        return self.name

class Test(models.Model):
    name = models.CharField(max_length=30)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)

    def __unicode__(self):
        return self.course.name +": "+self.name

class Grade(models.Model):
    value = models.DecimalField(max_digits=2,decimal_places=1,default=Decimal('0.0'))
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    test = models.ForeignKey(Test, on_delete=models.CASCADE)


    def __unicode__(self):
        return self.student.name + " - Curso: " + self.test.course.name +" - Prueba: "+ self.test.name + ": "+str(self.value)



