# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView
)

from .models import (
    Teacher,
    Student,
    Grade,
    Test,
    Course
)

from .forms import CourseForm

# Create your views here.
#Course
class CoursesList(ListView):
    model = Course

class CourseDetail(DetailView):
    model = Course

class CourseCreate(CreateView):
    form_class = CourseForm
    model = Course
    success_url = reverse_lazy('listcourse')

class CourseDelete(DeleteView):
    model = Course
    success_url = reverse_lazy('listcourse')

class CourseUpdate(UpdateView):
    form_class = CourseForm
    model = Course
    success_url = reverse_lazy('listcourse')
    #fields = ['name', 'teacher']

#Teacher
class TeachersList(ListView):
    model = Teacher

class TeacherDetail(DetailView):
    model = Teacher

class TeacherCreate(CreateView):
    model = Teacher
    success_url = reverse_lazy('listteacher')
    fields = ['name']

class TeacherDelete(DeleteView):
    model = Teacher
    success_url = reverse_lazy('listteacher')

class TeacherUpdate(UpdateView):
    model = Teacher
    success_url = reverse_lazy('listteacher')
    fields = ['name']

#Student
class StudentsList(ListView):
    model = Student

class StudentDetail(DetailView):
    model = Student

class StudentCreate(CreateView):
    model = Student
    success_url = reverse_lazy('liststudent')
    fields = ['name']

class StudentDelete(DeleteView):
    model = Student
    success_url = reverse_lazy('liststudent')

class StudentUpdate(UpdateView):
    model = Student
    success_url = reverse_lazy('liststudent')
    fields = ['name']

#Test
class TestsList(ListView):
    model = Test

class TestDetail(DetailView):
    model = Test


class TestCreate(CreateView):
    model = Test
    success_url = reverse_lazy('listtest')
    fields = ['name', 'course']


class TestDelete(DeleteView):
    model = Test
    success_url = reverse_lazy('listtest')


class TestUpdate(UpdateView):
    model = Test
    success_url = reverse_lazy('listtest')
    fields = ['name', 'course']

#grade
class GradesList(ListView):
    model = Grade

class GradeDetail(DetailView):
    model = Grade


class GradeCreate(CreateView):
    model = Grade
    success_url = reverse_lazy('listgrade')
    fields = ['value', 'student', 'test']


class GradeDelete(DeleteView):
    model = Grade
    success_url = reverse_lazy('listgrade')


class GradeUpdate(UpdateView):
    model = Grade
    success_url = reverse_lazy('listgrade')
    fields = ['value', 'student', 'test']
